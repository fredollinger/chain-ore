CC=rustc
TARGET=chain-ore
EDITOR=vim

$(TARGET):
	cargo build
clean:
	cargo clean

edit:
	$(EDITOR) src/main.rs

test: $(TARGET)
	cargo run
