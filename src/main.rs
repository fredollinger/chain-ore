// Import (via `use`) the `fmt` module to make it available.
use std::fmt;
use std::string;

// Try `write!` to see if it errors. If it errors, return
// the error. Otherwise continue.
// try!(write!(f, "{}", value));

// struct Complex { real: 3.3, imag: 7.2 }

#[derive(Debug)]
struct Complex {
    real: f64,
    imag: f64,
}

impl fmt::Display for Complex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "real: {}, imag: {}", self.real, self.imag)
    }
}

#[derive(Debug)]
struct List {
    string: std::string::String,
}

impl fmt::Display for List {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "string: {}", self.string)
    }
}

// This is a simple macro named `say_hello`.
macro_rules! say_hello {
    // `()` indicates that the macro takes no argument.
    () => (
        // The macro will expand into the contents of this block.
        println!("Hello!");
    )
}

fn main() {
    std::string::String code = "(red blue green)".to_string();
    println!("Hello! {}", code);
    //let l = List { string: "Hello".to_string() };
    //println!("Hello! {}", l);
}
